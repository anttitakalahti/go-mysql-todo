package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"todo/models"
)

func AddTask(todo models.TaskJSON) (id int64, err error) {
	db, err := sql.Open("mysql", "todo@/todo?charset=utf8")
	defer db.Close()

	if err != nil {
		return
	}

	res, err := db.Exec("INSERT INTO task (user, label) VALUES (?, ?)", todo.User, todo.Label)
	if err == nil {
		id, err = res.LastInsertId()
		if err != nil {
			fmt.Println("Error:", err)
		}
	}

	return
}

func GetTasks(userId int) []models.Task {
	db, err := sql.Open("mysql", "todo@/todo?charset=utf8")
	checkErr(err)
	defer db.Close()

	var tasks []models.Task
	rows, err := db.Query("SELECT id, user, label, completed FROM task WHERE user=?", userId)
	checkErr(err)

	for rows.Next() {
		var id int
		var user int
		var label string
		var completed int
		err = rows.Scan(&id, &user, &label, &completed)
		checkErr(err)
		tasks = append(tasks, models.NewTask(id, user, label, completed))
	}

	return tasks
}

func UpdateTask(taskId int, completed models.CompletedJSON) bool {
	db, err := sql.Open("mysql", "todo@/todo?charset=utf8")
	checkErr(err)
	defer db.Close()

	res, err := db.Exec("UPDATE task SET completed = ? WHERE id = ?", completed.Completed, taskId)
	if err == nil {
		rowsAffected, err := res.RowsAffected()
		if err != nil {
			fmt.Println("Error:", err)
			return false
		}

		if rowsAffected > 0 {
			return true
		} else {
			r, err := db.Query("SELECT count(*) FROM task WHERE id = ?", taskId)
			if err != nil {
				fmt.Println("Error:", err)
				return false
			}
			for r.Next() {
				var count int
				err = r.Scan(&count)
				if count > 0 {
					return true
				}
			}
		}
	}

	return false
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}
