package handler

import (
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"
	"todo/models"
)

func TestAddNewTaskUnit(t *testing.T) {
	if !testing.Short() {
		t.Skip("skipping unit test")
	}

	regularAddTask := addTask
	addTask = mockAddTask
	defer func() { addTask = regularAddTask }()

	bodyReader := strings.NewReader("{\"user\": 1, \"label\": \"A New Item\"}")
	r := httptest.NewRequest("POST", "http://example.com/task", bodyReader)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	assertInt(t, 201, w.Result().StatusCode)
	assertString(t, "/task/5", w.Header().Get("location"))
}

func TestListTasksUnit(t *testing.T) {
	if !testing.Short() {
		t.Skip("skipping unit test")
	}

	regularGetTasks := getTasks
	getTasks = mockGetTask
	defer func() { getTasks = regularGetTasks }()

	r := httptest.NewRequest("GET", "http://example.com/todo", nil)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	assertResponseBody(t, "[{11 1 Foo 0} {22 1 Bar 1}]", w.Result().Body)
}

func TestUpdateTaskUnit(t *testing.T) {
	if !testing.Short() {
		t.Skip("skipping unit test")
	}

	regularUpdateTask := updateTask
	updateTask = mockUpdateTask
	defer func() { updateTask = regularUpdateTask }()

	bodyReader := strings.NewReader("{\"completed\": true}")
	r := httptest.NewRequest("PUT", "http://example.com/task/1", bodyReader)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	if w.Result().StatusCode != 200 {
		t.Fatal("Expected status code 200, but got:", w.Result().StatusCode)
	}
}

func mockAddTask(todo models.TaskJSON) (int64, error) {
	if todo.User == 1 && todo.Label == "A New Item" {
		return 5, nil
	}
	return 0, fmt.Errorf("Expected user: 1, label: A New Item but got user: %s, label %s", todo.User, todo.Label)
}

func mockGetTask(id int) []models.Task {
	var tasks []models.Task
	tasks = append(tasks, models.NewTask(11, id, "Foo", 0))
	tasks = append(tasks, models.NewTask(22, id, "Bar", 1))
	return tasks
}

func mockUpdateTask(taskId int, completed models.CompletedJSON) bool {
	return taskId == 1
}
