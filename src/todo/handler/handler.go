package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"todo/database"
	"todo/models"
)

func NotFound(w http.ResponseWriter, _ *http.Request) {
	errorHandler(w, http.StatusNotFound)
}

func errorHandler(w http.ResponseWriter, status int) {
	w.WriteHeader(status)
	if status == http.StatusNotFound {
		fmt.Fprint(w, "custom 404")
	}
}

var addTask = database.AddTask
var getTasks = database.GetTasks
var updateTask = database.UpdateTask

func TaskHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "GET":
		getHandler(w, req)
	case "POST":
		postHandler(w, req)
	case "PUT":
		putHandler(w, req)
	case "DELETE":
		deleteHandler(w, req)
	default:
		NotFound(w, req)
	}
}

func getHandler(w http.ResponseWriter, req *http.Request) {
	switch req.URL.RequestURI() {
	case "/todo":
		listItems(w, req)
	default:
		NotFound(w, req)
	}
}

func postHandler(w http.ResponseWriter, req *http.Request) {
	switch req.URL.RequestURI() {
	case "/task":
		decoder := json.NewDecoder(req.Body)
		var todo models.TaskJSON
		err := decoder.Decode(&todo)
		if err != nil {
			errorHandler(w, http.StatusBadRequest)
			return
		}
		addItem(w, todo)
	default:
		NotFound(w, req)
	}
}

func putHandler(w http.ResponseWriter, req *http.Request) {
	match, _ := regexp.MatchString("^/task/[0-9]+$", req.URL.RequestURI())
	if !match {
		fmt.Println("NOT FOUND!")
		NotFound(w, req)
	} else {
		idString := req.URL.RequestURI()[6:]
		id, err := strconv.Atoi(idString)
		if err != nil {
			fmt.Println("strconv.Atoi("+idString+") failed with an error:", err)
			errorHandler(w, http.StatusBadRequest)
			return
		}

		decoder := json.NewDecoder(req.Body)
		var completed models.CompletedJSON
		err = decoder.Decode(&completed)
		if err != nil {
			fmt.Println("strconv.Atoi("+idString+") failed with an error:", err)
			errorHandler(w, http.StatusBadRequest)
			return
		}

		updateItem(w, id, completed)
	}
}

func deleteHandler(w http.ResponseWriter, req *http.Request) {
	NotFound(w, req)
}

func addItem(w http.ResponseWriter, todo models.TaskJSON) {
	id, err := addTask(todo)
	if err != nil {
		errorHandler(w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(201)
	w.Header().Set("location", fmt.Sprintf("/task/%d", id))
}
func listItems(w http.ResponseWriter, req *http.Request) {
	tasks := getTasks(1)
	fmt.Fprintln(w, tasks)
}
func updateItem(w http.ResponseWriter, id int, completed models.CompletedJSON) {
	success := updateTask(id, completed)
	if success {
		w.WriteHeader(200)
	} else {
		NotFound(w, nil)
	}

}
func deleteItem(w http.ResponseWriter, req *http.Request) {}
