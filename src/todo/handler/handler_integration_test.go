package handler

import (
	"bytes"
	"fmt"
	"io"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestAddNewTaskIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	bodyReader := strings.NewReader("{\"user\": 1, \"label\": \"A New Item\"}")
	r := httptest.NewRequest("POST", "http://example.com/task", bodyReader)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	assertInt(t, 201, w.Result().StatusCode)
	assertString(t, "/task/4", w.Header().Get("location"))
}

func TestListTasksIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	r := httptest.NewRequest("GET", "http://example.com/todo", nil)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	assertResponseBody(t, "[{1 1 Foo 0} {2 1 Bar 1} {4 1 A New Item 0}]", w.Result().Body)
}

func TestUpdateTaskIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	bodyReader := strings.NewReader("{\"completed\": true}")
	r := httptest.NewRequest("PUT", "http://example.com/task/1", bodyReader)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	assertInt(t, 200, w.Result().StatusCode)
}

func TestUpdateTaskWithBadIdIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	bodyReader := strings.NewReader("{\"completed\": true}")
	r := httptest.NewRequest("PUT", "http://example.com/task/666", bodyReader)
	w := httptest.NewRecorder()
	TaskHandler(w, r)

	assertInt(t, 404, w.Result().StatusCode)
}

func assertInt(t *testing.T, expected int, actual int) {
	if expected != actual {
		message := fmt.Sprintf("Expected %d but received %d.", expected, actual)
		t.Fatal(message)
	}
}

func assertResponseBody(t *testing.T, expected string, bodyReadCloser io.ReadCloser) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(bodyReadCloser)
	s := buf.String()

	expected = strings.TrimSpace(expected)
	actual := strings.TrimSpace(s)

	if expected != actual {
		message := fmt.Sprintf("Expected '%v' but received '%v'.", expected, actual)
		t.Fatal(message)
	}
}

func assertString(t *testing.T, expected string, actual string) {
	if expected != actual {
		message := fmt.Sprintf("Expected %d but received %d.", expected, actual)
		t.Fatal(message)
	}
}
