package models

type Task struct {
	Id 		int
	User	 	int
	Label 		string
	Completed 	int
}

func NewTask(id int, user int, label string, completed int) Task {
	return Task{id, user, label, completed}
}
