package main

import (
	"fmt"
	"net/http"
	"time"
	"todo/handler"
)

const (
	port = ":3000"
)

func init() {
	t := time.Now()
	fmt.Printf("%s - Started server at http://localhost%v.\n", t.Format(time.RFC3339), port)
	http.HandleFunc("/todo", handler.TaskHandler)
	http.HandleFunc("/", handler.NotFound)
	http.ListenAndServe(port, nil)
}

func main() {}
