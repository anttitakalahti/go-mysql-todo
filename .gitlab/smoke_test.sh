#!/bin/bash

EXPECTED="[{1 1 Foo 1} {2 1 Bar 1} {4 1 A New Item 0}]"
ACTUAL=$(curl --silent localhost:3000/todo)

if [ "$ACTUAL" != "$EXPECTED" ]; then
    echo "Wrong output! Expected: $EXPECTED but got: $ACTUAL"
    cat log/todo.out
    cat log/todo.err
    exit 1
fi
