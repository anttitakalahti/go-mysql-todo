DROP DATABASE IF EXISTS todo;
CREATE DATABASE todo;
use todo;
CREATE TABLE task (id int PRIMARY KEY NOT NULL AUTO_INCREMENT, user int, label VARCHAR(255), completed tinyint DEFAULT 0);
INSERT INTO task (id, user, label, completed) VALUES (1,1,'Foo', 0);
INSERT INTO task (id, user, label, completed) VALUES (2,1,'Bar', 1);
INSERT INTO task (id, user, label, completed) VALUES (3,2,'Baz', 0);
CREATE USER IF NOT EXISTS 'todo'@'localhost';
GRANT ALL ON todo.* TO 'todo'@'localhost';

