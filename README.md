

# Setup

```bash
    mysql -u root < .gitlab/test_database.sql
```

# Running
1. go get ./...
2. go install todo
3. bin/todo

# Unit tests
go test -v todo/... -short

# Integration tests
go test -v todo/...

# TODO
- stop server command (so that the server can be stopped by any owner)
- implement the actual functionality
